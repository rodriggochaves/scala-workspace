Compilando um programa:

```bash
$ scalac -d <pasta das classes> <caminho do arquivo>
```

Executando:

```bash
$ scala -classpath <caminho do arquivo> <nome do object> 
```